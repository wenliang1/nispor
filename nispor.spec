Name:           nispor
Version:        1.0.1
Release:        3%{?dist}
Summary:        API for network status querying
License:        ASL 2.0
URL:            https://github.com/nispor/nispor
Source:         https://github.com/nispor/nispor/archive/v%{version}.tar.gz#/%{name}-%{version}.tar.gz
Source1:        %{name}-%{version}-vendor.tar.xz
BuildRequires:  pkg-config
BuildRequires:  python3-devel
BuildRequires:  rust-toolset
BuildRequires:  systemd-rpm-macros
BuildRequires:  systemd-devel

%description
Unified interface for Linux network state querying.

%package -n     python3-%{name}
Summary:        %{summary}
Requires:       nispor = %{?epoch:%{epoch}:}%{version}-%{release}
BuildArch:      noarch

%description -n python3-%{name}

This package contains python3 binding of %{name}.

%package        devel
Summary:        %{summary}
Requires:       nispor%{?_isa} = %{?epoch:%{epoch}:}%{version}-%{release}

%description devel

This package contains C binding of %{name}.

%prep
%autosetup -p1

# Source1 is vendored dependencies
%cargo_prep -V 1

# The cargo_prep will create `.cargo/config` which take precedence over
# `.cargo/config.toml` shipped by upstream which fix the SONAME of cdylib.
# To workaround that, merge upstream rustflags into cargo_prep created one.
_FLAGS=`sed -ne 's/rustflags = "\(.\+\)"/\1/p' .cargo/config.toml`
sed -i -e "s/rustflags = \[\(.\+\), \]$/rustflags = [\1, \"$_FLAGS\"]/" \
    .cargo/config

%build
make
pushd src/python
%py3_build
popd

%post
/sbin/ldconfig
%systemd_post nispor.service

%preun
%systemd_preun nispor.service

%postun
/sbin/ldconfig
%systemd_postun_with_restart nispor.service

%install
env SKIP_PYTHON_INSTALL=1 PREFIX=%{_prefix} LIBDIR=%{_libdir} %make_install

pushd src/python
%py3_install
popd

%files
%doc AUTHORS CHANGELOG DEVEL.md README.md
%license LICENSE
%{_bindir}/npc
%{_bindir}/npd
%{_libdir}/libnispor.so.*
%{_unitdir}/nispor.socket
%{_unitdir}/nispor.service

%files -n       python3-%{name}
%license LICENSE
%{python3_sitelib}/nispor*

%files devel
%license LICENSE
%{_libdir}/libnispor.so
%{_includedir}/nispor.h
%{_libdir}/pkgconfig/nispor.pc

%changelog
* Fri Apr 16 2021 Mohan Boddu <mboddu@redhat.com> - 1.0.1-3
- Rebuilt for RHEL 9 BETA on Apr 15th 2021. Related: rhbz#1947937

* Fri Nov 13 2020 Gris Ge <fge@redhat.com> - 1.0.1-2
- Upload new cargo vendor tarbal without crates used for windows platform.

* Tue Nov 10 2020 Fernando Fernandez Mancera <ferferna@redhat.com> - 1.0.1-1
- Upgrade to 1.0.1

* Mon Nov 09 2020 Gris Ge <fge@redhat.com> - 1.0.0-1
- Upgrade to 1.0.0

* Mon Oct 19 2020 Gris Ge <fge@redhat.com> - 0.6.1-2
- Rebuild to load the compose settings.

* Tue Oct 13 2020 Gris Ge <fge@redhat.com> - 0.6.1-1
- Upgrade to 0.6.1

* Fri Oct 09 2020 Fernando Fernandez Mancera <ferferna@redhat.com> - 0.6.0-2
- VXLAN Python fix incorrect destination port

* Fri Oct 09 2020 Fernando Fernandez Mancera <ferferna@redhat.com> - 0.6.0-1
- Upgrade to 0.6.0

* Sun Sep 20 2020 Gris Ge <fge@redhat.com> - 0.5.1-1
- Upgrade to 0.5.1

* Thu Sep 17 2020 Gris Ge <fge@redhat.com> - 0.5.0-3
- Trigger rebuild for CI gating

* Tue Sep 08 2020 Gris Ge <fge@redhat.com> - 0.5.0-2
- Fix the python3-nispor requirement

* Fri Sep 04 2020 Gris Ge <fge@redhat.com> - 0.5.0-1
- Upgrade to 0.5.0

* Sat Aug 29 2020 Gris Ge <fge@redhat.com> - 0.4.0-3
- Fix the ldconfig

* Wed Aug 26 2020 Gris Ge <fge@redhat.com> - 0.4.0-2
- Add ldconfig to post and postun scripts

* Wed Aug 26 2020 Gris Ge <fge@redhat.com> - 0.4.0-1
- Upgrade to 0.4.0

* Mon Aug 17 2020 Gris Ge <fge@redhat.com> - 0.3.0-2
- Fix python linux bridge vlan filtering

* Sun Aug 16 2020 Gris Ge <fge@redhat.com> - 0.3.0-1
- Upgrade to 0.3.0

* Tue Aug 11 2020 Gris Ge <fge@redhat.com> - 0.2.0-1
- Upgrade to 0.2.0

* Thu Jul 09 2020 Gris Ge <fge@redhat.com> - 0.1.1-2
- Include license and documents

* Wed Jul 08 2020 Gris Ge <fge@redhat.com> - 0.1.1-1
- Initial build.
